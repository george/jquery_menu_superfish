<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/meta');

// Installation et mise à jour
function jquerysuperfish_upgrade($nom_meta_version_base, $version_cible){

	$maj = array();

	include_spip('inc/config');
   $maj = array();
	$maj['create'] = array(
			array('ecrire_config','jquerysuperfish', array(
				'menu_hori' => '',
				'menu_vert' => '',
				'menu_navbar' => ''
	)));
	$maj['0.3'] = array(
		array('jquerysuperfish_supprimer_supersubs')
	);	
	include_spip('base/upgrade');
   maj_plugin($nom_meta_version_base, $version_cible, $maj);
}


function jquerysuperfish_supprimer_supersubs() {
	$conf_jquerysuperfish = lire_config('jquerysuperfish');
	unset($conf_jquerysuperfish['supersubs']);
	unset($conf_jquerysuperfish['supersubs_minwidth']);
	unset($conf_jquerysuperfish['supersubs_maxwidth']);
	unset($conf_jquerysuperfish['supersubs_extrawidth']);
	ecrire_config('jquerysuperfish',$conf_jquerysuperfish);
}
// Désinstallation
function jquerysuperfish_vider_tables($nom_meta_version_base){

   effacer_meta('jquerysuperfish');
	effacer_meta($nom_meta_version_base);
}

?>
