<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'configurer_explication' => 'Consultez le <a href="https://superfish.joelbirch.design/examples/" target="_blank">site du plugin jQuery SuperFish</a> pour une démonstration de ses possibilités.',
	'configurer_titre' => 'Configurer jQuery Superfish',

	// E
	'erreur_generique' => 'Il y a des erreurs dans les champs ci-dessous, veuillez vérifier vos saisies',
	'erreur_min_max' => 'La valeur max doit être supérieure à la valeur min',
	'explication_animation'=>'Un objet equivalent au premier paramètre de la méthode jQuery .animate() permettant d\'animer les sous-menus. Par exemple <strong>opacity:\'show\',height:\'show\'</strong>, équivaut à une animation fade-in et slide-down',
	'explication_classe'=>'Classe de l\'élément ul sur lequel portera le menu',
	'explication_delai'=>'Le délai en millisecondes entre la sortie du pointeur du sous-menu et la fermeture de celui-ci',
	'explication_menu'=>'Cliquez sur oui pour activer et paramétrer un menu @type@ sur une arborescence ul/li',

	// L
	'label_animation' => 'Animation',
	'label_classe' => 'Classe',
	'label_delai' => 'Délai',
	'label_menu' => 'Gérer un menu @type@',
	'legend_menu' => 'Menu @type@' ,

	// T
	'texte_tester' => 'Si la configuration est enregistrée, vous pouvez <a href="@lien@">tester ce type de menu</a>' ,
	'titre_menu' => 'jQuery Superfish',
);
?>